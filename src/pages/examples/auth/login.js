import wxappUser from '@caomei/wxapp-user';
import { autoOpenSettingWhenNeedAuth } from '../../../utils/openSetting';
import env from '../../../env';

Page({
  data: {
    userInfo: null,
  },
  async login() {
    try {
      const userInfo = await autoOpenSettingWhenNeedAuth(() => wxappUser.login({
        force: true,
        serverOptions: {
          apiUrl: env.API_URL,
          appId: env.APP_ID,
        },
      }));
      this.setData({
        userInfo,
      });
    } catch (err) {
      this.setData({
        userInfo: err,
      });
      console.error(err);
    }
  },
  async loginWithOutServerInfo() {
    try {
      const userInfo = await autoOpenSettingWhenNeedAuth(() => wxappUser.login({
        force: true,
      }));
      this.setData({
        userInfo,
      });
    } catch (err) {
      this.setData({
        userInfo: err,
      });
      console.error(err);
    }
  },
});
