
function easeInOutExpo(t, b, c, d) {
  t /= d / 2;
  if (t < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;

  return c / 2 * (-Math.pow(2, -10 * (t - 1)) + 2) + b;
}

function getX(t, r) { // 由弧度得到 X 坐标
  return 100 + r * (16 * Math.pow(Math.sin(t), 3));
}

function getY(t, r) { // 由弧度得到 Y 坐标
  return 70 - r * (13 * Math.cos(t) - 5 * Math.cos(2 * t) - 2 * Math.cos(3 * t) - Math.cos(4 * t));
}
export default class HeartDraw {
  constructor(ctx, width, height) {
    this.ctx = ctx;
    this.width = width;
    this.height = height;
    this.points = Array(10).fill(undefined).map(() => (100 + Math.round(Math.random() * 20)));
    this.offsets = [];
    this.time = 0;
    this.totalTime = 20;
    this.space = Math.floor(width / this.points.length);
    this.value = 100;
    this.radio = 1;
    const heartPointNumber = 60;
    const radianDecrement = (Math.PI / heartPointNumber) * 2;
    const heartR = 5;

    let radian = Math.PI;// 弧度设为初始弧度
    this.heartPoints = Array(heartPointNumber).fill(undefined).map(() => {
      radian += radianDecrement;
      return [getX(radian, heartR), getY(radian, heartR)];
    });
    this.heartRandom = Array(heartPointNumber).fill(undefined)
      .map(() => Math.round(10 - (Math.random() * 20)));

    // ctx.setGlobalAlpha(0.7);
    // ctx.setFillStyle('#353535');
    this.animate = this.animate.bind(this);
    this.start();
  }
  nextFrame(value) {
    this.radio = Math.round((((value - this.value) / this.value) + 1) * 1.5) || 1;
    this.value = value;
    this.points[0] = Math.round(value);
  }
  animate() {
    const { ctx } = this;

    ctx.setStrokeStyle('red');
    ctx.setLineWidth(2);
    // ctx.fillRect(0, 0, this.width, this.height);
    this.drawHeart();
    // this.draw();
    ctx.stroke();// 画线
    ctx.draw();
    this.intervalIndex = setTimeout(this.animate, 100);
  }
  start() {
    this.stop();
    this.animate();
  }
  stop() {
    clearTimeout(this.intervalIndex);
  }
  drawHeart() {
    const {
      ctx, heartPoints, heartRandom, radio,
    } = this;

    for (let i = 0; i < heartPoints.length; i += 1) {
      const [x, y] = heartPoints[i];
      if (i === 0) {
        ctx.moveTo(x, y + (heartRandom[i] * radio));
      } else {
        ctx.lineTo(x, y + (heartRandom[i] * radio));
      }
    }

    heartRandom.pop();
    heartRandom.unshift(Math.round(10 - (Math.random() * 20)));
  }
  draw() {
    const {
      ctx, points, space,
    } = this;

    for (let i = 0; i < points.length; i += 1) {
      if (i === 0) {
        ctx.moveTo(0, points[i]);
      } else {
        ctx.lineTo(space * i, points[i]);
      }
    }

    points.pop();
    points.unshift(100 + Math.round(Math.random() * 20));
  }
}
